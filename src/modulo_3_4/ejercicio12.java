package modulo_3_4;

import java.util.Scanner;

public class ejercicio12 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Coloc� un n�mero entre 1 y 36 --> ");
		int num = scan.nextInt();
		if(num>=1 && num<=12) {
			System.out.println("El n�mero "+num+" est� en la primer docena");
		}
		else if(num>=13 && num<=24) {
			System.out.println("El n�mero "+num+" est� en la segunda docena");
		}
		else if(num>=25 && num<=36) {
			System.out.println("El n�mero "+num+" est� en la tercer docena");
		}
		else if(num<1 || num>36) {
			System.out.println("El n�mero "+num+" no est� listado");
		}
		scan.close();
	}
}
