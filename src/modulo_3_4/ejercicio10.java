package modulo_3_4;

import java.util.Scanner;

public class ejercicio10 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num1, num2, num3;
		System.out.print("Coloc� el primer n�mero --> ");
		num1 = scan.nextInt();
		System.out.print("Coloc� el segundo n�mero --> ");
		num2 = scan.nextInt();
		System.out.print("Coloc� el tercer n�mero --> ");
		num3 = scan.nextInt();
		if(num1==num2 && num2==num3) {
			System.out.println("Los tres n�meros son iguales");
		}
		else if(num1>num2 && num2==num3) {
			System.out.println("El primer n�mero ingresado "+num1+" es el mayor");
		}
		else if(num2>num3 && num1==num3) {
			System.out.println("El segundo n�mero ingresado "+num2+" es el mayor");
		}
		else if(num3>num1 && num1==num2) {
			System.out.println("El tercer n�mero ingresado "+num3+" es el mayor");
		}
		else if(num1==num2 && num1>num3) {
			System.out.println("El primer y segundo n�mero ingresados "+num1+" son los mayores");
		}
		else if(num1==num3 && num3>num2) {
			System.out.println("El primer y tercer n�mero ingresados "+num1+" son los mayores");
		}
		else if(num2==num3 && num2>num1) {
			System.out.println("El segundo y tercer n�mero ingresados "+num2+" son los mayores");
		}
		else if(num1>num2 && num2>num3) {
			System.out.println("El primer n�mero ingresado "+num1+" es el mayor");
		}
		else if(num1<num2 && num2<num3) {
			System.out.println("El tercer n�mero ingresado "+num3+" es el mayor");
		}
		else if(num1<num2 && num2>num3) {
			System.out.println("El segundo n�mero ingresado "+num2+" es el mayor");
		}
		scan.close();

	}
}
