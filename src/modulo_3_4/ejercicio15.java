package modulo_3_4;

import java.util.Scanner;

public class ejercicio15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Coloque la clase de su auto --> ");
		char auto = scan.next().charAt(0);
		
		switch(auto) {
		case 'A':
			System.out.println("Su auto tiene --> 4 ruedas | Motor");
			break;
		 case 'B':
			System.out.println("Su auto tiene --> 4 ruedas | Motor | Cerradura centralizada | A.C.");
			break;
		case 'C':
			System.out.println("Su auto tiene --> 4 ruedas | Motor | Cerradura centralizada | A.C. | AirBag");
			break;
		default:
			System.out.println("Error");
			break;
		}
		scan.close();
	}
}
