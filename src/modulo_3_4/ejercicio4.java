package modulo_3_4;

import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Coloque su categor�a --> "); 
		String cat = scan.nextLine();
		if(cat.equals("a")) {
			System.out.println("La categor�a es de hijo/a.");
		}
		else if(cat.equals("b")){
			System.out.println("La categor�a es de padre/madre.");
		}
		else if(cat.equals("c")) {
			System.out.println("La categor�a es de abuelo/abuela.");
		}
		else {
			System.out.println("La categor�a no existe");
		}
		scan.close();

	}

}
